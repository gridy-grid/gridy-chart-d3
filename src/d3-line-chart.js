
import { D3Chart } from "./d3-chart.js";

export class D3LineChart extends D3Chart {
	
	get reqFields() {
		if (! this._reqFields) {
			this._reqFields = ['x', 'y'];
		}
		return this._reqFields;
	}
	
	fill(d) {
		return this.el.confValOrDefault('fill', 'none');
	}
	
	stroke(d) {
		return this.colorByValue(d.x);
	}
	
	strokeWidth() {
		return this.el.confValOrDefault('stroke-width', 2.5);
	}
	
	opacity() {
		return this.el.confValOrDefault('opacity', 1);
	}
	
	render(target, fmtedData) {

		this.svg = d3.select(target)
			.append("svg")
			.attr("width", this.width + this.margin.left + this.margin.right)
			.attr("height", this.height + this.margin.top + this.margin.bottom)
			.append("g")
			.attr("transform", `translate(${this.margin.left},${this.margin.top})`);
		
		this.x = d3.scaleLinear()
			.domain(d3.extent(fmtedData, function (d) {
				return d.x;
			}))
			.range([0, this.width]);

		this.svg.append("g")
			.attr("transform", `translate(0, ${this.height})`)
			.call(d3.axisBottom(this.x).ticks(fmtedData.length));

		this.y = d3.scaleLinear()
			.domain([0, d3.max(fmtedData, function (d) {
				return +d.y;
			})])
			.range([this.height, 0]);

		this.svg.append("g")
			.call(d3.axisLeft(this.y));
			
        this.buildColorsByValue(fmtedData, 'x');

        this.res = this.svg.append("path")
            .datum(fmtedData)
            .attr("fill", this.fill.bind(this))
            .attr("stroke-width", this.strokeWidth.bind(this))
            .attr("stroke", this.stroke.bind(this))
			.style("opacity", this.opacity.bind(this))
	        .attr("d", this.fill() === 'none' 
		        ? d3.line() // just line
		            .x((d) => { return this.x(d.x) })
		            .y((d) => { return this.y(d.y) })
		        : d3.area() // filled
			        .x((d) => { return this.x(d.x) })
			        .y0(this.y(0))
			        .y1((d) => { return this.y(d.y) })
	        )
        if (this.el.hasAttribute('tooltip')) {
            this.renderTooltip(target, this.res, this.el.getAttribute('tooltip'));     
        }
		return this.svg;
	}
}
