
import { ResLoader } from "../../sk-core/complets/res-loader.js";

import { GridyChartDriver } from "../../gridy-grid/src/chart/gridy-chart-driver.js";

import { D3BarChart } from "./d3-bar-chart.js";
import { D3HistogramChart } from "./d3-histogram-chart.js";
import { D3GroupLineChart } from "./d3-group-line-chart.js";
import { D3LineChart } from "./d3-line-chart.js";
import { D3PieChart } from "./d3-pie-chart.js";
import { D3ScatterChart } from "./d3-scatter-chart.js";
import { DIMPORTS_AN } from "../../sk-core/src/sk-config.js";

export class GridyChartD3 extends GridyChartDriver {
    
    get chartClasses() {
        if (! this._chartClasses) {
            this._chartClasses = {
                'pie': 'D3PieChart',
                'bar': 'D3BarChart',
                'hist': 'D3HistogramChart',
                'line': 'D3LineChart',
                'groupline': 'D3GroupLineChart',
                'scatter': 'D3ScatterChart'
            };
        }
        return this._chartClasses;
    }
    
    set chartClasses(chartClasses) {
        this._chartClasses = chartClasses;
    }
    
    get chartTypePrefix() {
        if (! this._chartTypePrefix) {
            return this.el.hasAttribute('chart-type-prefix') 
                ? this.el.getAttribute('chart-type-prefix') : '/node_modules/gridy-chart-d3/src';
        }
        return this._chartTypePrefix;
    }
    
    set chartTypePrefix(chartTypePrefix) {
        this._chartTypePrefix = chartTypePrefix;
    }
    
    async implByType(type) {
        if (! this._implByType) {
            let implClassName = this.chartClasses[type];
            let implPath = this.chartTypePrefix + '/' + this.el.camelCase2Css(implClassName) + '.js';
            let forceInstance = this.el.hasAttribute('share-chart-impl') ? false : true;
            let dynImportsOff = (this.el.confValOrDefault(DIMPORTS_AN, null) === "false");
            this._implByType = ResLoader.dynLoad(implClassName, implPath, function(def) {
                return new def(this.el);
            }.bind(this), forceInstance, false, false ,false, false, false, dynImportsOff);
        } 
        return this._implByType;
    }
            

	async renderChart(el, data) {

		this.type = el.getAttribute('type');
	
		this.chartImpl = await this.implByType(this.type);
		this.chartImpl.configFromEl(el);

		let fmtedData = await this.chartImpl.fmtData(data);

		let container = el.querySelector('.gridy-chart-container');
		container.innerHTML = '';

		this.chart = this.chartImpl.render(container, fmtedData);
        return Promise.resolve(this.chart);
	}
}
		
