
import { D3Chart } from "./d3-chart.js";



export class D3PieChart extends D3Chart {
	
	get reqFields() {
		if (! this._reqFields) {
			this._reqFields = ['value'];
		}
		return this._reqFields;
	}
    
    get idxFields() {
		if (! this._idxFields) {
			this._idxFields = ['value'];
		}
		return this._idxFields;
	}
	
	data(fmtedData) {
		let pie = d3.pie()
			.value(function(d) {
				return d;
			});
		let values = pie(fmtedData.map((x) => x.value ));
		return values;
	}
	
	fill(d) {
		return this.colorByValue(d.value);
	}
	
	stroke() {
		return this.el.confValOrDefault('stroke', '#000000');
	}
	
	strokeWidth() {
		return this.el.confValOrDefault('stroke-width', 2.5);
	}  
    	
	innerRadius() {
		return this.el.confValOrDefault('inner-radius', 0);
	}
	
	radius() {
		return this.el.confValOrDefault('radius', Math.min(this.width, this.height) / 2 - 10);
	}
	
	render(target, fmtedData) {

		this.svg = d3.select(target)
			.append("svg")
			.attr("width", this.width + this.margin.left + this.margin.right)
			.attr("height", this.height + this.margin.top + this.margin.bottom)
			.append("g")
			.attr("transform", `translate(${this.width / 2},${this.height /  2})`);

        this.buildColorsByValue(fmtedData);
        
		this.res = this.svg
			.selectAll('whatever')
			.data(this.data(fmtedData))
			.join('path')
			.attr('d', d3.arc()
				.innerRadius(this.innerRadius.bind(this))
				.outerRadius(this.radius.bind(this))
			)
			.attr('fill', (d) => this.fill(d))
			.attr("stroke", this.stroke.bind(this))
			.style("stroke-width", this.strokeWidth.bind(this))
			.style("opacity", this.opacity.bind(this));
        if (this.el.hasAttribute('tooltip')) {
            this.renderTooltip(target, this.res, this.el.getAttribute('tooltip'));     
        }
		return this.svg;
	}
}
