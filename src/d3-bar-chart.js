
import { D3Chart } from "./d3-chart.js";


export class D3BarChart extends D3Chart {
	
	get reqFields() {
		if (! this._reqFields) {
			this._reqFields = ['name', 'value'];
		}
		return this._reqFields;
	}
		
	fill(d) {
		return this.colorByValue(d.value);
	}
	
    render(target, fmtedData) {
        //this.margin = {top: 30, right: 30, bottom: 70, left: 60};

        this.svg = d3.select(target)
            .append("svg")
            .attr("width", this.width + this.margin.left + this.margin.right)
            .attr("height", this.height + this.margin.top + this.margin.bottom)
            .append("g")
            .attr("transform", `translate(${this.margin.left},${this.margin.top})`);

        this.x = d3.scaleBand()
            .range([ 0, this.width ])
            .domain(fmtedData.map(d => d.name))
            .padding(0.2);
		// labels
        this.svg.append("g")
            .attr("transform", `translate(0, ${this.height})`)
            .call(d3.axisBottom(this.x))
            .selectAll("text")
            .attr("transform", "translate(-10,0)rotate(-45)")
            .style("text-anchor", "end");

        this.y = d3.scaleLinear()
            .domain([0, d3.max(fmtedData, function(d) { return +d.value })])
            .range([ this.height, 0]);
        
        this.svg.append("g")
            .call(d3.axisLeft(this.y));
        
        this.buildColorsByValue(fmtedData);
        
        this.res = this.svg.selectAll("mybar")
            .data(fmtedData)
            .join("rect")
            .attr("x", d => this.x(d.name))
            .attr("y", d => this.y(d.value))
            .attr("width", this.x.bandwidth())
            .attr("height", d => this.height - this.y(d.value))
            .attr('fill', this.fill.bind(this));
            
        if (this.el.hasAttribute('tooltip')) {
            this.renderTooltip(target, this.res, this.el.getAttribute('tooltip'));     
        }
        return this.svg;
    }
}
