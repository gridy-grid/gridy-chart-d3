
import { D3Chart } from "./d3-chart.js";

export class D3ScatterChart extends D3Chart {
	
	get reqFields() {
		if (! this._reqFields) {
			this._reqFields = ['x', 'y'];
		}
		return this._reqFields;
	}
	
	fill(d) {
		return this.el.confValOrDefault('fill', '#000000');
	}
    
    radius(d) {
		return this.el.confValOrDefault('radius', 1.5);
	}
	
	//stroke(d) {
		//return this.colorByValue(d.x);
	//}
	
	opacity() {
		return this.el.confValOrDefault('opacity', 1);
	}
	
	render(target, fmtedData) {

        this.svg = d3.select(target)
            .append("svg")
                .attr("width", this.width + this.margin.left + this.margin.right)
                .attr("height", this.height + this.margin.top + this.margin.bottom)
            .append("g")
                .attr("transform",
                      "translate(" + this.margin.left + "," + this.margin.top + ")");
		this.x = d3.scaleLinear()
            .domain([0, d3.max(fmtedData, function (d) {
				return +d.x;
			})])
            .range([ 0, this.width ]);
        this.svg.append("g")
            .attr("transform", "translate(0," + this.height + ")")
            .call(d3.axisBottom(this.x));

        this.y = d3.scaleLinear()
            .domain([0, d3.max(fmtedData, function (d) {
				return +d.y;
			})])
            .range([ this.height, 0]);
        this.svg.append("g")
            .call(d3.axisLeft(this.y));
          
        this.res = this.svg.append('g')
            .selectAll("dot")
            .data(fmtedData)
            .enter()
            .append("circle")
              .attr("cx", function (d) { return this.x(d.x); }.bind(this) )
              .attr("cy", function (d) { return this.y(d.y); }.bind(this) )
              .attr("r", (d) => this.radius(d))
              .style("fill", (d) => this.fill(d))
              .style("opacity", this.opacity.bind(this));
              
        if (this.el.hasAttribute('tooltip')) {
            this.renderTooltip(target, this.res, this.el.getAttribute('tooltip'));     
        } 
		return this.svg;
    }

	
}
