
import { D3Chart } from "./d3-chart.js";


export class D3HistogramChart extends D3Chart {
	
	get reqFields() {
		if (! this._reqFields) {
			this._reqFields = ['value'];
		}
		return this._reqFields;
	}
			
	fill(d) {
		return this.colorByClosestGreater(d.x1);
	}
	
    render(target, fmtedData) {
        //this.margin = { top: 10, right: 30, bottom: 30, left: 40 };
        
        this.svg = d3.select(target)
            .append("svg")
            .attr("width", this.width + this.margin.left + this.margin.right)
            .attr("height", this.height + this.margin.top + this.margin.bottom)
            .append("g")
            .attr("transform", `translate(${this.margin.left},${this.margin.top})`);

        this.x = d3.scaleLinear()
            .domain([0, d3.max(fmtedData, function(d) { return +d.value })])
            .range([0, this.width]);
        this.svg.append("g")
            .attr("transform", `translate(0, ${this.height})`)
            .call(d3.axisBottom(this.x));

        let histogram = d3.histogram()
            .value(function(d) { return d.value; })  
            .domain(this.x.domain())  
            .thresholds(this.x.ticks(fmtedData.length)); 

        let bins = histogram(fmtedData);

        this.y = d3.scaleLinear()
            .range([this.height, 0]);
        
        this.y.domain([0, d3.max(bins, function(d) { return d.length; })]); 
        
        this.svg.append("g")
            .call(d3.axisLeft(this.y));
            
        this.buildColorsByValue(fmtedData);    

        this.res = this.svg.selectAll("rect")
            .data(bins)
            .join("rect")
            .attr("x", 1)
            .attr("transform", (d) => { return `translate(${this.x(d.x0)} , ${this.y(d.length)})`})
            .attr("width", (d) => { return this.x(d.x1) - this.x(d.x0) -1})
            .attr("height", (d) => { return this.height - this.y(d.length); })
            .attr('fill', this.fill.bind(this));
        if (this.el.hasAttribute('tooltip')) {
            this.renderTooltip(target, this.res, this.el.getAttribute('tooltip'));     
        }
		return this.svg;
    }
    
    colorByClosestGreater(value) {
		let color = '#000000';
		let newValue = value; 
		for (let colorValue of Object.keys(this.colorsByValue)) {
			colorValue = parseInt(colorValue);
			if (colorValue >= value) {
				if (colorValue <= newValue || newValue === value) {
				    newValue = colorValue;
				}
			}
		}
		return this.colorsByValue[newValue] ? this.colorsByValue[newValue] : this.el.confValOrDefault('color', '#000000');
	}
    
}
