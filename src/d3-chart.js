

import { FieldMappedChart } from "../../gridy-grid/src/chart/impl/field-mapped-chart.js";


export class D3Chart extends FieldMappedChart {
    
    get margin() {
        if (! this._margin) {
            this._margin = { 
                top: this.attrOrDefault('margin-top', 20),
                right: this.attrOrDefault('margin-right', 20),
                bottom: this.attrOrDefault('margin-bottom', 20),
                left: this.attrOrDefault('margin-left', 30)
            }
        }
        return this._margin;
    }
    
    set margin(margin) {
        this._margin = margin;
    }
    
    get width() {
        if (! this._width) {
            this._width = this.el.width - this.margin.left - this.margin.right;
        }
        return this._width; 
    }
    
    set width(width) {
        this._width = width;
    }
    
    get height() {
        if (! this._height) {
            this._height = this.el.height - this.margin.top - this.margin.bottom;
        }
        return this._height; 
    }
        
    set height(height) {
        this._height = height;
    }
    
    opacity() {
        return parseFloat(this.el.confValOrDefault('opacity', 1));
    }
    
    get defaultTooltip() {
        let fields = [];
        for (let fieldKey of Object.keys(this.fieldMappings)) {
            fields.push(`${fieldKey} = {{ ${fieldKey} }}`);
        }
        return fields.join(', ');
    }
            
    renderTooltip(target, res, tplStr) {
        this.tooltip = d3.select(target)
            .append("div")
            .style("opacity", 0)
            .attr("class", "gridy-tooltip")
            .style("position", "absolute")
            .style("background-color", "white")
            .style("background-color", "white")
            .style("border", "solid")
            .style("border-width", "1px")
            .style("border-radius", "5px")
            .style("padding", "10px");
        let mouseover = function(event, d) {
            this.tooltip.style("opacity", 1);
        }.bind(this);

        let mousemove = function(event, d) {
            this.tooltip.style("opacity", 1);
            if (! tplStr) {
                tplStr = this.defaultTooltip;
            }
            let ctx = d;
            if (this.byField['value'] && this.byField['value'].get(d.value)) {
                let fieldData = this.byField['value'].get(d.value);
                ctx = {...d, ...fieldData};
            }
            this.tooltip
                .html(this.el.format(tplStr, ctx))
                  .style("left", (event.x)/2 + "px") 
                  .style("top", (event.y)/2 + "px")
        }.bind(this);
        
        let mouseleave = function(event, d) {
            this.tooltip
              .transition()
              .duration(200)
              .style("opacity", 0)
        }.bind(this);
        
        res.on("mouseover", mouseover)
            .on("mousemove", mousemove)
            .on("mouseleave", mouseleave);    
    }
}
