# GridyGrid chart component D3 driver


## Installation

```shell
npm i gridy-grid d3 gridy-chart-d3 gridy-grid-default sk-theme-default
```

and plug it to your page

```html
<script src="/node_modules/d3/dist/d3.js"></script>
```

or from CDN

```html
<script src="https://cdn.jsdelivr.net/npm/d3@7"></script>
```

then add element to your container or setup it programmatically

```html
    <gridy-grid id="gridyGrid" base-path="/node_modules/gridy-grid/src" sort-field="$.title">
	    <gridy-data-source fields='[{ "title": "Name", "path": "$.name" },{ "title": "Value", "path": "$.value"},{ "title": "Color", "path": "$.color"}]'
                       datasource-type="DataSourceLocal" datapath="$.data"></cbr-data-source>
    
	<gridy-chart type="pie" dri="d3" field-name="Name" field-color="Color" field-value="Value" width="400" height="400"></cbr-chart>
    
    </gridy-grid>
    <script>
        let data = [];
	    data.push({ name: 'FooBar1', value: 10, color: '#98abc5' });
	    data.push({ name: 'FooBar2', value: 23, color: '#8a89a6' });
	    data.push({ name: 'FooBar3', value: 34, color: '#7b6888' });
	    data.push({ name: 'FooBar4', value: 17, color: '#a05d56' });
        let grid = document.querySelector('#gridyGrid');
        grid.addEventListener('bootstrap', () => {
            grid.charts[0].addEventListener('skrender', (event) => {
                grid.dataSource.loadData(data);
            });
        });
    </script>
    
```

to map datasource data for chart field-${dataKey} attributes are used with
field title from datasource mapping as value  


## Chart types

### Pie

```html
	<gridy-chart type="pie" dri="d3" field-color="Color" field-value="Value" width="400" height="400"></cbr-chart>
```

config attrs: stroke, stroke-width, radius, inner-radius

### Bar 

```html
	<gridy-chart type="bar" dri="d3" field-color="Color" field-name="Name" field-value="Value" width="400" height="400"></cbr-chart>
```

config attrs: color (fill)

### Histogram

```html
	<gridy-chart type="hist" dri="d3" field-color="Color" field-name="Name" field-value="Value" width="400" height="400"></cbr-chart>
```

config attrs: color (fill)

### Line

```html
	<gridy-chart stroke-width="5" color="#f0f0f0" fill="#f0f0f0" type="line" dri="d3" field-x="x" field-y="y" width="400" height="400"></cbr-chart>
```

config attrs: color (stroke), stroke-width, fill

### Groupline

groupline chart allows multiple lines for data grouped by value of specified field

```html
	<gridy-chart stroke-width="5" color="#f0f0f0" fill="#f0f0f0" type="groupline" dri="d3" field-x="x" field-y="y" field-group="Name" width="400" height="400"></cbr-chart>
```

config attrs: color (stroke), stroke-width

## Configuration

### attributes

share-chart-dri - share chart driver instance from cache than instancinate, ok when all types of chart on page have the same type

share-chart-impl - prefer get chart impl from cache than instancinate, ok when all types of chart on page have the same type
